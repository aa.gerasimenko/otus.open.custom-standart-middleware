using customStandart.Middlewares;

var builder = WebApplication.CreateBuilder(args);

// Add services to the container.

builder.Services.AddControllers();
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();
builder.Services.AddSwaggerGen();

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}
app.Map("/throw", builder => throw new Exception("throw"));

app.Use(async (httpContext, next) =>
{
    try
    {
        await next(httpContext);
    }
    catch (Exception exception)
    {
        var h = @$"<html>
        <strong>Trace:</strong>
        <br/>{exception.StackTrace}

        <br/><br/><br/>
        <strong>Message:</strong>
        <br/>{exception.Message}
        <html>";
        httpContext.Response.ContentType = "text/html";
        await httpContext.Response.WriteAsync(h);
    }
});

//app.UseHttpsRedirection();
app.UseMiddleware<CustomHttpsRedirection>();

// app.UseAuthentication(); // who a u
// app.UseAuthorization();  // what can do
app.UseMiddleware<CustomAuthenticationMiddleware>();
app.UseMiddleware<CustomAuthorizationMiddleware>();

app.UseMiddleware<CustomStaticFileMiddleware>();

app.MapControllers();

app.Run();
