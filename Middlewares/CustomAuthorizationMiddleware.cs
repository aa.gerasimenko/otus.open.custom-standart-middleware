using System.Security.Claims;
using Microsoft.AspNetCore.Http;

namespace customStandart.Middlewares;

public class CustomAuthorizationMiddleware
{
    private readonly RequestDelegate _next;
    private readonly int _authenticatedCode = StatusCodes.Status401Unauthorized;
    private readonly int _authorizedCode = StatusCodes.Status403Forbidden;

    public CustomAuthorizationMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task InvokeAsync(HttpContext context)
    {
        var endpoint = context.GetEndpoint();
        if (endpoint == null)
        {
            await _next(context);
            return;
        }
        if (endpoint.DisplayName?.ToLowerInvariant().Contains("protected") == false)
        {
            await _next(context);
            return;
        }

        var identity = context.User.Identity;
        if (identity == null || identity.IsAuthenticated == false)
        {
            context.Response.StatusCode = _authenticatedCode;
            await context.Response.WriteAsync("not authenticated");
            return;
        }
        var role = context.User.Claims.FirstOrDefault(claim => claim.Type == "role");
        if (role?.Value != "admin")
        {
            context.Response.StatusCode = _authorizedCode;
            await context.Response.WriteAsync("not authorized");
            return;
        }

        await _next.Invoke(context);
    }
}