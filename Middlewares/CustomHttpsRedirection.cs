using Microsoft.AspNetCore.Http.Extensions;
using Microsoft.Extensions.Primitives;

namespace customStandart.Middlewares;

public class CustomHttpsRedirection
{
    private readonly RequestDelegate _next;
    private readonly int _sPort = 7115;
    private readonly int _code = StatusCodes.Status307TemporaryRedirect;

    public CustomHttpsRedirection(RequestDelegate next)
    {
        _next = next;
    }

    public async Task InvokeAsync(HttpContext context)
    {
        if (context.Request.IsHttps)
        {
            await _next(context);
            return;
        }

        var url = UriHelper.BuildAbsolute(
            "https",
            new HostString(context.Request.Host.Host, _sPort),
            context.Request.PathBase,
            context.Request.Path,
            context.Request.QueryString
        );

        context.Response.Headers["Location"] = new StringValues(url);
        context.Response.StatusCode = _code;
    }
}