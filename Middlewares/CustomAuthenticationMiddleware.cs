using System.Security.Claims;

namespace customStandart.Middlewares;

public class CustomAuthenticationMiddleware
{
    private readonly RequestDelegate _next;

    public CustomAuthenticationMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task InvokeAsync(HttpContext context)
    {
        var qp = context.Request.Query;
        var claims = qp.Select(p => new Claim(p.Key, p.Value)).ToList();
        if (claims.Count > 0)
        {
            context.User = new ClaimsPrincipal(
                new ClaimsIdentity(claims, "cookie")
            );
        }

        await _next.Invoke(context);
    }
}