namespace customStandart.Middlewares;

public class Custom
{
    private readonly RequestDelegate _next;

    public Custom(RequestDelegate next)
    {
        _next = next;
    }

    public async Task InvokeAsync(HttpContext context)
    {
        await _next.Invoke(context);
    }
}