namespace customStandart.Middlewares;

public class CustomStaticFileMiddleware
{
    private readonly RequestDelegate _next;

    public CustomStaticFileMiddleware(RequestDelegate next)
    {
        _next = next;
    }

    public async Task InvokeAsync(HttpContext context)
    {
        var path = context.Request.Path.Value; // relative path
        if (path.StartsWith("/app"))
        {
            var filename = path.Replace("/app/", "static/");
            await context.Response.SendFileAsync(filename);
            return;
        }

        await _next.Invoke(context);
    }
}